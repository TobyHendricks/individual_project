﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Measures the position of a vehicle in the race
/// </summary>
public class RacePositionHandler : MonoBehaviour
{
    public Action<int> OnFinishedRace;

    public WaypointTrack track;
    public float waypointRadius = 15;

    public Transform waypointPointer;

    private int lap;
    private int currentIndex;
    private Transform curWaypoint;
    private int startingIndex;
    private int numPoints;
    private int numWaysToStart = 0;
    private float distToWay;
    private int numLapsInRace;

    private float nextPossibleLap = 0;

    public int waypointsToStart { get { return numWaysToStart; } }
    public float distanceToWaypoint { get { return distToWay; } }
    public int currentLap { get { return lap; } }


    private void Start()
    {
        startingIndex = track.ClosestWaypointIndex(transform.position);
        SetWaypoint(startingIndex);
        numPoints = track.waypoints.Count;
    }

    // Update is called once per frame
    void Update()
    {
        if (waypointPointer != null)
        {
            waypointPointer.LookAt(curWaypoint.position);
            waypointPointer.gameObject.SetActive(Vector3.Angle(transform.forward, curWaypoint.position - transform.position) > 50);
        }

        distToWay = Vector3.Distance(curWaypoint.position, transform.position);
        if (distToWay < waypointRadius)
        {
            SetWaypoint(track.NextIndex(currentIndex));
        }
    }

    /// <summary>
    /// Sets the current waypoint to the index i
    /// </summary>
    /// <param name="i"></param>
    private void SetWaypoint(int i)
    {
        currentIndex = i;
        curWaypoint = track.GetWaypoint(i);
        numWaysToStart = CalculateNumWaysToStart(i);
    }

    /// <summary>
    /// Sets the number of laps in the race
    /// </summary>
    /// <param name="l"></param>
    public void SetNumLaps(int l)
    {
        numLapsInRace = l;
    }

    /// <summary>
    /// Called when the vehicle enters a Unity trigger zone. In this case, it handles the vehicle crossing the finish line
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "FinishLine" && currentIndex == startingIndex && nextPossibleLap < Time.time)
        {
            nextPossibleLap = Time.time + 5;

            if (lap == numLapsInRace)
            {
                int place = RaceManager.OnCarFinish();
                OnFinishedRace?.Invoke(place);
            }

            ++lap;
            Debug.Log("Lap");
        }
    }

    /// <summary>
    /// Returns true if another vehicle is ahead
    /// </summary>
    /// <param name="otherLap">The current lap of the other vehicle</param>
    /// <param name="otherNumWaysToStart">The other vehicle's number of waypoints to the start</param>
    /// <param name="otherDistToWay">The other vehicle's distance to its current waypoint</param>
    public bool IsOtherAhead(int otherLap, int otherNumWaysToStart, float otherDistToWay)
    {
        float myDist = Vector3.Distance(transform.position, curWaypoint.position);

        if (otherLap > lap)
            return true;
        else if (otherLap < lap)
            return false;

        if (otherNumWaysToStart < numWaysToStart)
            return true;
        else if (otherNumWaysToStart > numWaysToStart)
            return false;

        if (otherDistToWay < myDist)
            return true;

        return false;
    }

    /// <summary>
    /// Returns true if another vehicle is ahead
    /// </summary>
    public bool IsOtherAhead(RacePositionHandler other)
    {
        return IsOtherAhead(other.currentLap, other.waypointsToStart, other.distanceToWaypoint);
    }

    /// <summary>
    /// Returns the number of waypoints to the start from waypoint index i
    /// </summary>
    private int CalculateNumWaysToStart(int i)
    {
        if (i == startingIndex)
            return 0;
        else if(i < startingIndex)
        {
            return startingIndex - i;
        }
        
        return (numPoints - i) + startingIndex;
    }
}
