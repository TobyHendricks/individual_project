﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A collection of waypoints, defining a track
/// </summary>
public class WaypointTrack : MonoBehaviour
{
    public List<Transform> waypoints = new List<Transform>();

    /// <summary>
    /// Gets a waypoint at index
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public Transform GetWaypoint(int index)
    {
        return waypoints[index % waypoints.Count];
    }
    
    /// <summary>
    /// Calculates the next index
    /// </summary>
    /// <param name="curIndex"></param>
    /// <returns></returns>
    public int NextIndex(int curIndex)
    {
        return (curIndex + 1) % waypoints.Count;
    }

    /// <summary>
    /// Returns the waypoint after the waypoint at the curIndex
    /// </summary>
    public Transform NextWaypoint(int curIndex)
    {
        return waypoints[NextIndex(curIndex)];
    }

    /// <summary>
    /// Finds the closest waypoint to position pos
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public int ClosestWaypointIndex(Vector3 pos)
    {
        int closestIndex = 0;
        float closestDistance = Vector3.Distance(pos, waypoints[0].position);

        for(int i = 1; i < waypoints.Count; ++i)
        {
            float d = Vector3.Distance(pos, waypoints[i].position);
            if(d < closestDistance)
            {
                closestDistance = d;
                closestIndex = i;
            }
        }

        Vector2 dirToCurrentWaypoint = new Vector2(waypoints[closestIndex].position.x, waypoints[closestIndex].position.z) - new Vector2(pos.x, pos.z);
        Vector2 dirToNextWaypoint = new Vector2(NextWaypoint(closestIndex).position.x, NextWaypoint(closestIndex).position.z) - new Vector2(pos.x, pos.z);

        if(Vector2.Dot(dirToCurrentWaypoint.normalized, dirToNextWaypoint.normalized) < 0)
        {
            closestIndex = NextIndex(closestIndex);
        }

        return closestIndex;
    }

    //----------Editor-----------
    #region Editor
    private void OnDrawGizmos()
    {
        for(int i = 0; i < waypoints.Count; ++i)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(waypoints[i].position, NextWaypoint(i).position);
        }
    }
    #endregion

}
