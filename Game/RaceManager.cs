﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manages a race between a main player and AIs
/// </summary>
public class RaceManager : MonoBehaviour
{
    public RacePositionHandler[] contestants;
    public RacePositionHandler player;
    public Text playerPositionText;
    public Text lapText;
    public int numLaps;
    public int playerPosition { get; private set; }

    private static int numFinishes;

    // Start is called before the first frame update
    void Start()
    {
        foreach(RacePositionHandler contestant in contestants)
        {
            contestant.gameObject.SetActive(true);
            contestant.SetNumLaps(numLaps);
        }

        numFinishes = 0;

        InvokeRepeating(nameof(UpdatePlayerPosition), 0.2f, 0.2f);
    }

    // Updates the players position in the race
    void UpdatePlayerPosition()
    {
        int playerPos = 1;

        foreach(RacePositionHandler contestant in contestants)
        {
            if (contestant.gameObject.GetInstanceID() == player.gameObject.GetInstanceID())
                continue;

            if (player.IsOtherAhead(contestant))
                ++playerPos;
        }

        playerPositionText.text = PositionToString(playerPos);
        playerPosition = playerPos;

        lapText.text = "Lap " + Mathf.Clamp(player.currentLap, 1, numLaps).ToString() + "/" + numLaps.ToString();
    }

    /// <summary>
    /// Converts a race position to a string
    /// </summary>
    public static string PositionToString(int pos)
    {
        if(pos != 11 && pos != 12 && pos != 13)
        {
            int m = pos % 10;
            if(m == 1)
            {
                return pos.ToString() + "st";
            }
            else if(m == 2)
            {
                return pos.ToString() + "nd";
            }
            else if (m == 3)
            {
                return pos.ToString() + "rd";
            }
        }

        return pos.ToString() + "th";
    }

    /// <summary>
    /// Called when a car finishes
    /// </summary>
    /// <returns>The place the car finishes</returns>
    public static int OnCarFinish()
    {
        numFinishes++;
        return numFinishes;
    }
}
