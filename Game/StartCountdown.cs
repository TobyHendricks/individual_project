﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Subjects vehicles to a countdown on start
/// </summary>
public class StartCountdown : MonoBehaviour
{
    public VehicleController[] contestants;
    public Text startText;

    private float countdown = 3;
    private bool started = false;

    // Start is called before the first frame update
    void Start()
    {
        foreach(VehicleController contestant in contestants)
        {
            contestant.SetFreeze(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;

        if(countdown > 0)
        {
            startText.text = Mathf.Ceil(countdown).ToString();
        }
        else
        {
            if(!started)
            {
                started = true;
                foreach(VehicleController contestant in contestants)
                {
                    contestant.SetFreeze(false);
                }

                startText.text = "GO";
            }

            if(countdown < -3)
            {
                Destroy(gameObject);
            }
        }
        
    }
}
