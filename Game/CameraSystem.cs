﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls the game camera
/// </summary>
public class CameraSystem : MonoBehaviour
{
    public Rigidbody target;
    private Camera cam;

    [Header("Follow")]
    public Vector2 offset;
    public float directionMultiplier = 4;
    public float speedHeightMultiplier = 0.01f;
    public float moveLerp = 10;
    public float rotationLerp = 10;

    [Header("FOV")]
    public float minFOV = 60;
    public float maxFOV = 110;
    public float speedMultiplier = 3;
    public float fovLerp = 10;

    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    //Fixed update is called by Unity a fixed number of times per second
    void FixedUpdate()
    {
        if (target == null)
            return;

        Vector3 targetLocation = target.transform.position + Vector3.up * (offset.y + target.velocity.magnitude * speedHeightMultiplier);
        targetLocation -= target.transform.forward * offset.x;
        transform.position = Vector3.Lerp(transform.position, targetLocation, moveLerp * Time.fixedDeltaTime);

        Vector3 vel = target.transform.forward;
        Vector3 lookDir = (target.transform.position + vel.normalized * directionMultiplier) - transform.position;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(lookDir), rotationLerp * Time.fixedDeltaTime);

        float targetFOV = Mathf.Clamp(target.velocity.magnitude * speedMultiplier, minFOV, maxFOV);
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, targetFOV, fovLerp * Time.fixedDeltaTime);
    }

    /// <summary>
    /// Sets the target of the camera
    /// </summary>
    public void SetTarget(Rigidbody t)
    {
        target = t;
    }
}
