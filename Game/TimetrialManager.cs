﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages a time trial
/// </summary>
public class TimetrialManager : MonoBehaviour
{
    public TimetrialHUD HUD;

    bool started = false;
    float curTime = 0;

    private float last = 0;
    private float best = Mathf.Infinity;
    // Update is called once per frame
    void Update()
    {
        if (started)
        {
            curTime += Time.deltaTime;
            HUD.SetCurrentTime(TimeToString(curTime));
        }
            
    }

    /// <summary>
    /// Called when a vehicle enters the trigger zone, in this case indicating that a vehicle has completed a lap
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        started = true;

        if (curTime > 1)
        {
            last = curTime;
            if(curTime < best)
            {
                best = curTime;
            }

            HUD.SetBestTime(TimeToString(best));
            HUD.SetLastTime(TimeToString(last));
        }

        curTime = 0;
    }

    /// <summary>
    /// Converts a time as a float to a string
    /// </summary>
    /// <param name="time"></param>
    /// <returns></returns>
    private string TimeToString(float time)
    {
        int minutes = Mathf.FloorToInt(time / 60);
        string minutesString = minutes < 10 ? "0" + minutes.ToString() : minutes.ToString();

        int seconds = Mathf.FloorToInt(time % 60);
        string secondsString = seconds < 10 ? "0" + seconds.ToString() : seconds.ToString();

        int point = Mathf.FloorToInt((time % 1) * 100);
        string pointString = point < 10 ? "0" + point.ToString() : point.ToString();

        return minutesString + ":" + secondsString + "." + pointString;
    }
}
