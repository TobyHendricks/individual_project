﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls tyre effects
/// </summary>
public class TyreFX : MonoBehaviour
{
    public Wheel wheelController;
    public AudioSource tyreAudio;
    public TrailRenderer skidMark;

    // Start is called before the first frame update
    void Start()
    {
        if (tyreAudio == null)
            tyreAudio = GetComponentInChildren<AudioSource>();

        if (skidMark == null)
            skidMark = GetComponentInChildren<TrailRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTyreFX();
    }

    private void UpdateTyreFX()
    {
        if (tyreAudio == null)
            return;

        if (tyreAudio.isPlaying && !wheelController.wheelSkidding)
            tyreAudio.Pause();
        else if (!tyreAudio.isPlaying && wheelController.wheelSkidding)
            tyreAudio.Play();

        skidMark.emitting = wheelController.wheelSkidding;
        skidMark.transform.position = wheelController.wheelContactPoint + Vector3.up * 0.1f;

    }
}
