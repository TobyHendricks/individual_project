﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the wheels 3d model, spinning and rotating it to match
/// </summary>
public class WheelModel : MonoBehaviour
{
    [Tooltip("The child spins, this object steers")]
    public Transform childModel;

    //Called by unity before the first frame update
    private void Start()
    {
        if (childModel == null)
            childModel = transform.GetChild(0);
    }

    /// <summary>
    /// Spins the wheel
    /// </summary>
    /// <param name="rate"></param>
    public void Spin(float rate)
    {
        if(!float.IsNaN(rate))
            childModel.transform.Rotate(new Vector3(rate, 0, 0), Space.Self);
    }

    /// <summary>
    /// Rotates the wheel
    /// </summary>
    /// <param name="localRot"></param>
    public void Steer(Quaternion localRot)
    {
        transform.localRotation = localRot;
    }
}
