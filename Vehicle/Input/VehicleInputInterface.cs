﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface VehicleInputInterface
{
    float ReadHorizontalInput();
    float ReadGasInput();
    bool ReadGearUpInput();
    bool ReadGearDownInput();
    bool ReadHandbrakeInput();
    float ReadBrakeInput();
}
