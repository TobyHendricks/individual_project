﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Reads the input using Unity's Input system
/// </summary>
public class StandardPlayerInput : MonoBehaviour, VehicleInputInterface
{
    public float ReadHorizontalInput()
    {
        return Mathf.Pow(Input.GetAxisRaw("Horizontal"), 3);
    }

    public float ReadGasInput()
    {
        return Input.GetAxisRaw("Accelerator");
    }

    public bool ReadGearUpInput()
    {
        return Input.GetButtonDown("GearUp");
    }

    public bool ReadGearDownInput()
    {
        return Input.GetButtonDown("GearDown");
    }

    public bool ReadHandbrakeInput()
    {
        return Input.GetButton("Handbrake");
    }

    public float ReadBrakeInput()
    {
        return Input.GetAxisRaw("Brake");
    }
}
