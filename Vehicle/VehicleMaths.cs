﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Useful Maths Equations
/// </summary>
public class VehicleMaths
{
    /// <summary>
    /// Returns a friction force based on the current momentum, the coefficient of friction and the normal force
    /// </summary>
    public static Vector3 CalculateFrictionForce(Vector3 momentum, float frictionCoefficient, float normalMagnitude)
    {
        float maxMag = momentum.magnitude / Time.fixedDeltaTime;
        Vector3 frictionForce = -momentum.normalized * frictionCoefficient * normalMagnitude;
        return Vector3.ClampMagnitude(frictionForce, maxMag);
    }

    /// <summary>
    /// Calculates scalar friction
    /// </summary>
    public static float CalculateFrictionMagnitude(float momentum, float frictionCoefficient, float normalMagnitude)
    {
        float maxMag = Mathf.Abs(momentum);
        float frictionMagnitude = -Mathf.Sign(momentum) * frictionCoefficient * normalMagnitude;
        return Mathf.Clamp(frictionMagnitude, -maxMag, maxMag);
    }
}
