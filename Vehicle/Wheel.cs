﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Calculates and applies tyre, wheel and suspension physics
/// </summary>
public class Wheel : MonoBehaviour
{
    //rigidbody of the car
    private Rigidbody carBody;
    [Tooltip("The 3d model representaiton of the wheel")]
    public WheelModel wheelModel;
    [Tooltip("The wheel on the opposite side")]
    public Wheel correspondingWheel;

    //The type of wheel
    public enum WheelType { Regular, Offroad}
    public WheelType wheelType;

    #region Suspension
    [Header("Suspension System")]
    public float wheelRestDistance = 0.3f;
    public float springRange = 0.05f;
    public float suspensionStiffness = 120000;
    public float suspensionDamping = 6000;
    public float antiRollBarStiffness = 35000;

    //Suspension variables
    private List<Vector2> rayOffsets = new List<Vector2>();

    private float minSpring;
    private float maxSpring;
    private float springLength;
    private float weightRatio = 0.25f;

    //Suspension properties
    public float normalisedSpringCompression { get { return (springLength - minSpring) / (maxSpring - minSpring); } }
    public float wheelWeightRatio
    {
        get { return weightRatio; }
        set { weightRatio = value; }
    }

    #endregion

    #region Wheel
    [Header("Wheel Properties")]
    public float radius = 0.3f;
    public float rollingResistance = 0.012f;
    public float wheelMass = 12;

    //Wheel variables
    private bool contacting;
    private Vector3 contactPoint;
    private Vector3 contactNormal;
    private float angularVelocity;
    private float slipRatio;
    private float rawSlipRatio;
    private float slipSign;
    private float slipAngle;
    private float lateralFrictionCoefficient = 0.8f;
    private float currentSurfaceTraction = 0.9f;
    private float currentDownforce;

    private bool handbrake;
    private bool skidding;

    private Surface currentSurface = Surface.Tarmac;

    //Wheel properties
    public bool wheelDriven { get; set; }
    public bool wheelBraked { get; set; }
    public bool wheelReversing { get; set; }
    public bool wheelContacting { get { return contacting; } }
    public Vector3 wheelContactPoint { get { return contactPoint; } }
    public Vector3 wheelContactNormal { get { return contactNormal; } }
    public bool wheelHandbrake { set { handbrake = value; } }
    public float wheelAngularVelocity { get { return angularVelocity; } }
    public bool wheelSkidding { get { return skidding && carBody.velocity.magnitude > 3; } }

    [Header("Surface Settings")]
    public float tarmacLateralFriction = 0.9f;
    public float tarmacTraction = 0.7f;
    public float dirtLateralFriction = 0.3f;
    public float dirtTraction = 0.3f;

    #endregion

    #region Settings

    [Header("Physics settings")]
    public int additionalWheelRays = 4;
    public float angleBetweenRays = 16;
    public LayerMask rayMask;

    [Header("Audio Settings")]
    public float slipRatioThreshold = 0.3f;
    public float slipAngleThreshold = 20;

    #endregion

    const float opposingResistance = 0.026f;

    // Start is called by Unity before the first frame update
    void Start()
    {
        carBody = GetComponentInParent<Rigidbody>();

        InitialiseSuspension();
    }

    /// <summary>
    /// Sets the opposite wheel
    /// </summary>
    public void SetOppositeWheel(Wheel oppositeWheel)
    {
        correspondingWheel = oppositeWheel;
    }

    /// <summary>
    /// Initialises the suspension
    /// </summary>
    private void InitialiseSuspension()
    {
        minSpring = wheelRestDistance - springRange;
        maxSpring = wheelRestDistance + springRange;

        int numRays = 1 + additionalWheelRays;
        rayOffsets.Add(Vector3.zero);

        for (int i = 1; i <= additionalWheelRays; ++i)
        {
            Vector2 newDir = Vector2.down.Rotate(angleBetweenRays * i) * radius;
            Vector2 difference = newDir - new Vector2(0, -radius);

            rayOffsets.Add(difference);
            rayOffsets.Add(new Vector2(-difference.x, difference.y));
        }
    }

    //Fixed update is called by Unity a fixed number of times per second
    void FixedUpdate()
    {
        ApplySuspension();
        UpdateWheelRotation(Time.fixedDeltaTime);
        ApplyTyreForces();
        ApplySpeedToAngularVelocity();
    }

    //Update is called by Unity every frame
    private void Update()
    {
        bool lateralSlip = (Mathf.Abs(slipAngle) > slipAngleThreshold && Mathf.Abs(slipAngle) < 180 - slipAngleThreshold);
        skidding = (rawSlipRatio > slipRatioThreshold || (lateralSlip && carBody.velocity.magnitude > 2)) 
                    && currentSurface == Surface.Tarmac
                    && contacting;
        UpdateSurfaceValues();
    }

    /// <summary>
    /// Updates the rotation of the wheel model
    /// </summary>
    /// <param name="delta">time between updates</param>
    private void UpdateWheelRotation(float delta)
    {
        wheelModel.Spin(angularVelocity * delta * Mathf.Rad2Deg);
        wheelModel.Steer(transform.localRotation);
    }

    /// <summary>
    /// Applies suspension forces to the rigidbody
    /// </summary>
    private void ApplySuspension()
    {
        RaycastHit hit;
        bool assigned = false;

        //Iterate through all the rays used to model the wheel
        foreach (Vector3 o in rayOffsets)
        {
            Vector3 rayOffset = new Vector3();
            rayOffset += transform.up * o.y;
            rayOffset += transform.forward * o.x;

            //Check if a surface is detected beneath the wheel
            if (!assigned && Physics.Raycast(transform.position + rayOffset, -transform.up, out hit, maxSpring + radius, rayMask))
            {
                //Spring physics
                float prevLength = springLength;

                springLength = Mathf.Clamp(hit.distance - radius, 0, maxSpring);

                float springVel = (prevLength - springLength) / Time.fixedDeltaTime;
                float springForce = suspensionStiffness * (wheelRestDistance - springLength);
                float dampingForce = suspensionDamping * springVel;

                //Spring force
                Vector3 suspensionForce = (springForce + dampingForce) * transform.up;
                carBody.AddForceAtPosition(suspensionForce, hit.point);

                //Anti roll bar
                if(correspondingWheel.contacting)
                {
                    float antirollCoefficient = Mathf.Clamp(correspondingWheel.normalisedSpringCompression - normalisedSpringCompression, 0, 1);
                    Vector3 antiRollForce = -correspondingWheel.transform.up * antiRollBarStiffness * antirollCoefficient;
                    carBody.AddForceAtPosition(antiRollForce, correspondingWheel.transform.position - correspondingWheel.transform.up * minSpring);
                    Debug.DrawRay(correspondingWheel.transform.position - correspondingWheel.transform.up * minSpring, antiRollForce * 0.001f);
                }


                //Store info about contact
                contacting = true;
                contactPoint = hit.point;
                contactNormal = hit.normal;
                currentSurface = TagToSurface(hit.transform.tag);

                //Position wheel
                wheelModel.transform.position = transform.position - transform.up * springLength;

                assigned = true;//Add a break statement in final
            }
        }

        //If there is no contact
        if (!assigned)
        {
            contacting = false;

            //extend wheel to max length
            wheelModel.transform.position = transform.position - transform.up * maxSpring;
            springLength = maxSpring;
        }

    }


    /// <summary>
    /// Applies friction forces to the rigidbody based on the tyres current rotation and the current slip ratio
    /// </summary>
    private void ApplyTyreForces()
    {
        if (!contacting)
            return;

        float normalForce = CalculateWheelLoad();
        slipRatio = CalculateSlipRatio();

        Vector3 wheelVelocity = carBody.GetPointVelocity(contactPoint);
        Vector3 planearWheelVelocity = Vector3.ProjectOnPlane(wheelVelocity, transform.up);
        float peakFriction = Mathf.Abs(normalForce) * currentSurfaceTraction;

        //Slip angle Calculation
        slipAngle = Vector3.SignedAngle(transform.forward, planearWheelVelocity, transform.up);
        if(Mathf.Abs(slipAngle) > 90)
            slipAngle = (180 - Mathf.Abs(slipAngle)) * Mathf.Sign(slipAngle);

        //Debugging
        Debug.DrawRay(contactPoint, wheelVelocity, Color.magenta);
        Debug.DrawRay(contactPoint, transform.forward * wheelVelocity.magnitude, Color.yellow);

        Vector3 resLatForce = Vector3.zero;
        Vector3 resLongForce = Vector3.zero;

        if (!handbrake)
        {
            //Lateral force
            Vector3 lateralForce = transform.right;
            float scalarLateralForce = DynamicPacejka(normalForce, slipAngle, currentSurface);
            resLatForce = transform.right * scalarLateralForce;

            //Debugging
            Debug.DrawRay(contactPoint, lateralForce / carBody.mass, Color.blue);
            
            if (wheelBraked || wheelDriven)
            {
                //longitudinal force
                float scalarLongitudinalForce = DynamicPacejka(normalForce, slipRatio * slipSign, currentSurface);
                resLongForce = -transform.forward * scalarLongitudinalForce;

                //Apply force to wheel, constrained by peak friction
                if(scalarLateralForce + scalarLongitudinalForce > peakFriction)
                {
                    float longRatio = scalarLongitudinalForce / (scalarLateralForce + scalarLongitudinalForce);
                    scalarLongitudinalForce = peakFriction * longRatio;
                }
                float torque = scalarLongitudinalForce * radius;
                float angularAccel = (torque / wheelMass) * Time.fixedDeltaTime;

                float speedDifference = CalculateForwardSignedSpeed() - (angularVelocity * radius);

                //Limit amount of angular acceleration
                if(Mathf.Sign(angularAccel) == Mathf.Sign(speedDifference))
                {
                    angularAccel = Mathf.Clamp(angularAccel, -Mathf.Abs(speedDifference), Mathf.Abs(speedDifference));
                }

                AddAngularAcceleration(angularAccel);


                //Debugging
                Debug.DrawRay(contactPoint, resLongForce / carBody.mass, Color.blue);

            }
        }
        else
        {   
            angularVelocity = 0;

            //Handbrake friction
            Vector3 lateralMomentum = planearWheelVelocity;
            lateralMomentum *= carBody.mass * wheelWeightRatio;
            resLatForce = VehicleMaths.CalculateFrictionForce(lateralMomentum, currentSurfaceTraction, Mathf.Abs(normalForce));

            //Debugging
            Debug.DrawRay(contactPoint, resLatForce * 0.5f / carBody.mass, Color.yellow);

            
        }

        //Constrain total force by peak friction
        Vector3 resultantForce = resLatForce + resLongForce;
        resultantForce = Vector3.ClampMagnitude(resultantForce, peakFriction);

        //Add force to rigidbody
        carBody.AddForceAtPosition(resultantForce, contactPoint);


        //rolling resistance friction
        Vector3 longitudinalVelocity = Vector3.ProjectOnPlane(planearWheelVelocity, transform.right);
        Vector3 longitudinalMomentum = longitudinalVelocity * carBody.mass * wheelWeightRatio;
        Vector3 longitudinalFriction = VehicleMaths.CalculateFrictionForce(longitudinalMomentum, rollingResistance, Mathf.Abs(normalForce));
        carBody.AddForceAtPosition(longitudinalFriction, contactPoint);
    }

    /// <summary>
    /// Brakes the wheels
    /// </summary>
    /// <param name="magnitude">amount of opposing torque to apply</param>
    public void ApplyBrakingTorque(float magnitude)
    {
        //amount of angular deceleration
        float decel = Mathf.Abs(magnitude * Time.fixedDeltaTime) / wheelMass;

        if (Mathf.Abs(angularVelocity) < decel)
        {
            angularVelocity = 0;
        }
        else
        {
           AddAngularAcceleration(-decel * Mathf.Sign(angularVelocity));
        }
    }

    /// <summary>
    /// Returns the forward velocity of the wheel
    /// </summary>
    public Vector3 GetForwardVelocity()
    {
        Vector3 v = Vector3.ProjectOnPlane(carBody.velocity, transform.right);
        v = Vector3.ProjectOnPlane(v, transform.up);
        return v;
    }

    /// <summary>
    /// Returns the absolute forward speed of the wheel
    /// </summary>
    public float CalculateForwardSpeed()
    {
        
        return GetForwardVelocity().magnitude;
    }

    /// <summary>
    /// Returns the signed forward speed (positive for forward, negative for backwards)
    /// </summary>
    public float CalculateForwardSignedSpeed()
    {
        Vector3 forwardVel = GetForwardVelocity();
        return forwardVel.magnitude * Vector3.Dot(forwardVel.normalized, transform.forward.normalized);
    }

    /// <summary>
    /// Returns the normal force
    /// </summary>
    private float CalculateWheelLoad()
    {
        return carBody.mass * Physics.gravity.y * wheelWeightRatio - currentDownforce;
    }

    /// <summary>
    /// Calculates the slip ratio of the wheel
    /// </summary>
    public float CalculateSlipRatio()
    {
        //Calculate ratio
        float f = CalculateForwardSignedSpeed();
        float ratio = -((f - radius * angularVelocity) / f);

        rawSlipRatio = Mathf.Abs(ratio);
        ratio = Mathf.Clamp(ratio, -1, 1);

        //Adjust sign to ensure force is applied in the correct direction
        if(Mathf.Abs(radius * angularVelocity) < Mathf.Abs(f))
        {
            ratio = Mathf.Abs(ratio) * -Mathf.Sign(f);
        }
        else
        {
            if (wheelReversing)
                ratio = -Mathf.Abs(ratio);
            else if (wheelDriven)
                ratio = Mathf.Abs(ratio);
            else
                ratio = Mathf.Abs(ratio) * Mathf.Sign(angularVelocity);
        }

        slipSign = 1;

        return ratio;
    }

    /// <summary>
    /// Returns the result of the pacejka equation
    /// </summary>
    /// <param name="fz">normal force</param>
    /// <param name="slip">slip ratio or angle</param>
    /// <param name="curSurface">surface that the tyres on</param>
    private float DynamicPacejka(float fz, float slip, Surface curSurface)
    {
        if(curSurface == Surface.Dirt)
        {
            return PacejkaDirt(fz, slip);
        }

        return PacejkaTarmac(fz, slip);
    }

    /// <summary>
    /// Returns the result of the pacejka equation for tarmac
    /// </summary>
    /// <param name="fz">normal force</param>
    /// <param name="slip">slip ratio or angle</param>
    private float PacejkaTarmac(float fz, float slip)
    {
        return Pacejka(fz, 10, 1.9f, currentSurfaceTraction, 0.97f, slip);
    }

    /// <summary>
    /// Returns the result of the pacejka equation for dirt
    /// </summary>
    /// <param name="fz">normal force</param>
    /// <param name="slip">slip ratio or angle</param>
    private float PacejkaDirt(float fz, float slip)
    {
        if(wheelType == WheelType.Offroad)
            return Pacejka(fz, 8, 2f, currentSurfaceTraction, 1, slip);
        else
            return Pacejka(fz, 6, 2f, currentSurfaceTraction, 1, slip);
    }

    /// <summary>
    /// Returns the result of the pacejka equation
    /// </summary>
    private float Pacejka(float fz, float stiffness, float shape, float peak, float curvature, float slip)
    {
        float inner = stiffness * slip - curvature * (stiffness * slip - (float)Math.Atan((double)stiffness * (double)slip));
        float output = fz * peak * (float)Math.Sin((double)shape * Math.Atan((double)inner));

        return output;
    }

    /// <summary>
    /// Calculates the resultant angular acceleration from the torque applied
    /// </summary>
    public float CalculateResultantAngularAcceleration(float torque)
    {
        return (torque / wheelMass) * Time.fixedDeltaTime;
    }

    /// <summary>
    /// Adds angular acceleration
    /// </summary>
    public void AddAngularAcceleration(float accel)
    {
        angularVelocity += accel;
    }
    
    /// <summary>
    /// Translates forward speed to equivelant angular velocity
    /// </summary>
    public void ApplySpeedToAngularVelocity()
    {
        if (!contacting || wheelWeightRatio < 0.01f || handbrake || wheelDriven || wheelReversing)
            return;

        float rotationalForwardSpeed = angularVelocity * radius;
        float forwardSpeed = CalculateForwardSignedSpeed();
        float momentumDifference = forwardSpeed - rotationalForwardSpeed;
        momentumDifference *= carBody.mass * wheelWeightRatio;

        float mag = opposingResistance;
        if (Mathf.Abs(rotationalForwardSpeed) < Mathf.Abs(forwardSpeed) || !wheelDriven)
            mag = currentSurfaceTraction;
        
        float frictionForce = VehicleMaths.CalculateFrictionMagnitude(momentumDifference, Mathf.Max(0.001f, mag), CalculateWheelLoad());
        float frictionTorque = frictionForce * radius;
        float acceleration = frictionTorque / (carBody.mass * wheelWeightRatio);
        angularVelocity += acceleration;
    }

    /// <summary>
    /// Set the downforce of the wheel
    /// </summary>
    /// <param name="df"></param>
    public void SetDownforce(float df)
    {
        currentDownforce = df;
    }

    /// <summary>
    /// Updates the tyre values depending on the current surface
    /// </summary>
    private void UpdateSurfaceValues()
    {
        switch(currentSurface)
        {
            case Surface.Tarmac:
                currentSurfaceTraction = tarmacTraction;
                lateralFrictionCoefficient = tarmacLateralFriction;
                break;
            case Surface.Dirt:
                currentSurfaceTraction = dirtTraction;
                lateralFrictionCoefficient = dirtLateralFriction;
                break;
        }

    }

    /// <summary>
    /// Converts the tag of an object to the surface
    /// </summary>
    /// <param name="tag"></param>
    /// <returns></returns>
    public static Surface TagToSurface(string tag)
    {
        switch (tag)
        {
            case "Dirt":
                return Surface.Dirt;
            case "Tarmac":
                return Surface.Tarmac;
            default:
                return Surface.Tarmac;
        }
    }

    #region Unity Editor
    //Called by unity for debugging
    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 1);
        Gizmos.DrawRay(transform.position, -transform.up * (wheelRestDistance + springRange + radius));

        Gizmos.color = new Color(1, 0, 0, 0.6f);

        if (contacting)
        {
            Gizmos.DrawSphere(contactPoint, 0.05f);
        }
        else
        {
            Gizmos.DrawSphere(transform.position - transform.up * wheelRestDistance, radius);
            Gizmos.color = new Color(0, 1, 0, 0.3f);
            Gizmos.DrawSphere(transform.position - transform.up * (wheelRestDistance - springRange), radius);
            Gizmos.color = new Color(0, 1, 1, 0.3f);
            Gizmos.DrawSphere(transform.position - transform.up * (wheelRestDistance + springRange), radius);
        }

        Gizmos.color = new Color(0, 0, 1, 0.6f);
        Gizmos.DrawSphere(transform.position + (-transform.up * (wheelRestDistance)), 0.05f);

        if (carBody != null)
        {
            //Gizmos.DrawRay(carBody.transform.position, carBody.velocity * 10);
        }
    }
    #endregion
}

public enum Surface { Tarmac, Dirt }
