﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleController : MonoBehaviour
{
    [Header("Components")]
    private Rigidbody carBody;
    private VehicleInputInterface inputManager;

    public Wheel frontLeftWheel;
    public Wheel frontRightWheel;
    public Wheel backLeftWheel;
    public Wheel backRightWheel;

    public Wheel[] driveWheels;
    public Wheel[] nonDriveWheels;
    public AudioSource engineAudio;

    public float speed { get { return carBody.velocity.magnitude; } }

    [Header("Steering")]
    public float maxSteerAngle;
    public float steerAdjustSpeed;

    //Steering variables
    private float steerAngle;
    private float wheelbase;

    [Header("Engine/Gears/Brakes")]
    public float minRpm;
    public float maxRpm;
    public AnimationCurve torqueCurve = AnimationCurve.Linear(0, 0, 8000, 500);
    public float[] gearRatios;
    public float finalDriveRatio;
    public float engineBrakingCoefficient = 0.74f;
    public float brakeTorque;

    //Engine variables
    public float curRpm;
    public int curGear = 0;
    public float gearChangeStamp;

    [Header("Aerodynamics")]
    public float dragCoefficient = 0.3f;
    public float dragArea = 1.8f;
    public float downforceCoefficient;
    public float downforceArea;
    public float slipstreamSpeedThreshold;
    public Collider[] slipstreamColliders;

    //Aero variables
    public float currentDownforce { get; private set; }
    private float slipstreamTimeout;
    public bool slipstreaming { get { return Time.time < slipstreamTimeout; } }

    [Header("Audio")]
    public float minPitch;
    public float maxPitch;

    [Header("Debug")]
    private float mph;
    public bool debugUI;

    public float currentGearRatio
    {
        get 
        {
            if (curGear >= 0)
                return gearRatios[curGear];
            else
                return -gearRatios[0];
        }
    }

    [HideInInspector]
    public bool frozen;
    
    // Start is called by Unity before the first frame update
    void Start()
    {
        carBody = GetComponent<Rigidbody>();
        inputManager = GetComponent<VehicleInputInterface>();
        if (inputManager == null)
            inputManager = gameObject.AddComponent<StandardPlayerInput>();

        foreach(Wheel w in nonDriveWheels)
        {
            w.wheelDriven = false;
        }

        curRpm = minRpm;

        InitialiseSteering();
    }

    /// <summary>
    /// Initialises the steering system
    /// </summary>
    private void InitialiseSteering()
    {
        wheelbase = Vector3.Distance(frontLeftWheel.transform.position, backLeftWheel.transform.position);

        frontLeftWheel.SetOppositeWheel(frontRightWheel);
        frontRightWheel.SetOppositeWheel(frontLeftWheel);
        backLeftWheel.SetOppositeWheel(backRightWheel);
        backRightWheel.SetOppositeWheel(backLeftWheel);
    }

    /// <summary>
    /// Resets the steering
    /// </summary>
    public void ResetSteering()
    {
        steerAngle = 0;
    }    


    //Update is called by Unity once per frame
    private void Update()
    {
        mph = (carBody.velocity.magnitude * 2.237f);
        int gr = curGear;

        if(Time.time > gearChangeStamp)
        {
            if(inputManager.ReadGearUpInput())
            {
                gr++;
                gearChangeStamp = Time.time + 0.15f;
            }

            if(inputManager.ReadGearDownInput())
            {
                gr--;
                gearChangeStamp = Time.time + 0.15f;
            }
        }


        curGear = Mathf.Clamp(gr, -1, gearRatios.Length - 1);

        if(curRpm > 0)
        {
            float normalisedRpm = (curRpm - minRpm) / (maxRpm - minRpm);
            engineAudio.pitch = (maxPitch - minPitch) * normalisedRpm + minPitch;
        }

        //Slipstream
        bool createSlipstream = carBody.velocity.magnitude > slipstreamSpeedThreshold;
        if (createSlipstream != slipstreamColliders[0].enabled)
        {
            foreach (Collider col in slipstreamColliders)
                col.enabled = createSlipstream;
        }
    }

    //Fixed update is called by Unity a fixed number of times per second
    void FixedUpdate()
    {
        float hInput = frozen? 0 : inputManager.ReadHorizontalInput();
        bool handBrake = frozen? true : inputManager.ReadHandbrakeInput();
        float gasInput = frozen? 0 : inputManager.ReadGasInput();
        float brakeInput = frozen? 0 : inputManager.ReadBrakeInput();

        CalculateWeightDistribution();
        Steer(hInput * maxSteerAngle, Time.fixedDeltaTime);
        
        ApplyHandBrake(handBrake);
        if(!handBrake)
        {
            ApplyEngineForces(gasInput);
            //ApplyEngineBrakingForce();
            ApplyBrakeForce(brakeInput);
        }

        ApplyAero();
    }

    /// <summary>
    /// Rotates the wheels towards a target angle
    /// </summary>
    /// <param name="target"></param>
    /// <param name="delta">Time between updates</param>
    private void Steer(float target, float delta)
    {
        //Calculate the maximum adjustment
        float steeringAdjustment = steerAdjustSpeed * delta;

        if(Mathf.Abs(steerAngle - target) > steeringAdjustment)
            steerAngle += steeringAdjustment * Mathf.Sign(target - steerAngle);
        else
            steerAngle = target;

        //Clamp
        steerAngle = Mathf.Clamp(steerAngle, -maxSteerAngle, maxSteerAngle);

        //Ackerman steering for the other wheel
        float circleRadius = wheelbase / Mathf.Sin(Mathf.Abs(steerAngle) * Mathf.Deg2Rad);
        float leftAngle = 0;
        float rightAngle = 0;

        if (steerAngle > 0.0001f)
        {
            Vector3 circleCenter = frontRightWheel.transform.position + frontRightWheel.transform.right * circleRadius;
            leftAngle = Vector3.Angle(frontLeftWheel.transform.position - circleCenter, backLeftWheel.transform.position - circleCenter);
            rightAngle = steerAngle;
        }
        else if(steerAngle < -0.0001f)
        {
            Vector3 circleCenter = frontLeftWheel.transform.position - frontLeftWheel.transform.right * circleRadius;
            rightAngle = -Vector3.Angle(frontRightWheel.transform.position - circleCenter, backRightWheel.transform.position - circleCenter);
            leftAngle = steerAngle;
        }

        frontRightWheel.transform.localRotation = Quaternion.Euler(new Vector3(0, rightAngle, 0));
        frontLeftWheel.transform.localRotation = Quaternion.Euler(new Vector3(0, leftAngle, 0));

    }

    /// <summary>
    /// Calculates and sets the weight distribution for each wheel
    /// </summary>
    private void CalculateWeightDistribution()
    {
        float denominator = 0;
        denominator += 1 - frontLeftWheel.normalisedSpringCompression;
        denominator += 1 - frontRightWheel.normalisedSpringCompression;
        denominator += 1 - backLeftWheel.normalisedSpringCompression;
        denominator += 1 - backRightWheel.normalisedSpringCompression;

        if(denominator > 0)
        {
            frontLeftWheel.wheelWeightRatio = (1 - frontLeftWheel.normalisedSpringCompression) / denominator;
            frontRightWheel.wheelWeightRatio = (1 - frontRightWheel.normalisedSpringCompression) / denominator;
            backLeftWheel.wheelWeightRatio = (1 - backLeftWheel.normalisedSpringCompression) / denominator;
            backRightWheel.wheelWeightRatio = (1 - backRightWheel.normalisedSpringCompression) / denominator;
        }
        else
        {
            frontLeftWheel.wheelWeightRatio = 0.25f;
            frontRightWheel.wheelWeightRatio = 0.25f;
            backLeftWheel.wheelWeightRatio = 0.25f;
            backRightWheel.wheelWeightRatio = 0.25f;
        }
    }

    /// <summary>
    /// Applys the engine forces to the wheels
    /// </summary>
    /// <param name="pedal"></param>
    private void ApplyEngineForces(float pedal)
    {

        curRpm = Mathf.Clamp(curRpm, minRpm, maxRpm);
        float wheelTorque = ((EvaluateTorqueCurve(curRpm) * currentGearRatio * finalDriveRatio * pedal) / driveWheels.Length);

        float wheelAcceleration = Mathf.Infinity;
        bool apply = false;

        foreach(Wheel w in driveWheels)
        {

            if(pedal > 0.1f && Time.time > gearChangeStamp)
            {
                //Find the wheel with the lowest acceleration
                float wheelAccel = w.CalculateResultantAngularAcceleration(wheelTorque);
                if(wheelAccel < wheelAcceleration)
                {
                    wheelAcceleration = wheelAccel;
                    apply = true;
                }

                w.wheelReversing = currentGearRatio < 0;
                w.wheelDriven = true;
            }
            else
            {
                w.wheelReversing = false;
                w.wheelDriven = false;
            }
        }
        
        //Apply that acceleration to each wheel
        if(apply && !OverLimit())
        {
            foreach (Wheel w in driveWheels)
            {
                w.AddAngularAcceleration(wheelAcceleration);
            }
        }

        if(speed > 2)
        {
            ApplyEngineBrakingForce();
        }

        //RPM calculation
        float minVel = Mathf.Infinity;
        foreach(Wheel w in driveWheels)
        {
            float unsignedVel = Mathf.Abs(w.wheelAngularVelocity);
            if(unsignedVel < minVel)
            {
                minVel = unsignedVel;
            }
        }
        curRpm = Mathf.Clamp(CalculateRPM(minVel), minRpm, maxRpm);
    }

    /// <summary>
    /// Applies engine braking forces
    /// </summary>
    private void ApplyEngineBrakingForce()
    {
        Vector3 forwardVel = Vector3.ProjectOnPlane(carBody.velocity, transform.right);
        carBody.AddForce(forwardVel.normalized * -engineBrakingCoefficient * (curRpm / 60));
    }

    /// <summary>
    /// Applies brake forces
    /// </summary>
    /// <param name="pedal"></param>
    private void ApplyBrakeForce(float pedal)
    {
        bool braking = pedal > 0.1f;
        foreach (Wheel w in driveWheels)
        {
            if(braking)
                w.ApplyBrakingTorque((-brakeTorque) * pedal);
            w.wheelBraked = braking;
        }
        foreach (Wheel w in nonDriveWheels)
        {
            if(braking)
                w.ApplyBrakingTorque((-brakeTorque) * pedal);
            w.wheelBraked = braking;
        }

    }

    /// <summary>
    /// Calculates if the wheels are spinning faster than the engine can handle
    /// </summary>
    /// <returns></returns>
    private bool OverLimit()
    {
        foreach (Wheel w in driveWheels)
        {
            if (CalculateRPM(w.wheelAngularVelocity) > maxRpm)
                return true;
        }

        return false;
    }

    /// <summary>
    /// Calculates the RPM from the wheel's angular velocity
    /// </summary>
    /// <param name="wheelVel">the wheel's angular velocity</param>
    /// <returns></returns>
    private float CalculateRPM(float wheelVel)
    {
        return (Mathf.Abs(wheelVel) * 60 * Mathf.Abs(currentGearRatio) * finalDriveRatio) / (2 * Mathf.PI);
    }

    /// <summary>
    /// Calculates what the RPM would be from the wheel's angular velocity for the gear
    /// </summary>
    /// <param name="wheelVel">the wheel's angular velocity</param>
    /// <param name="gear"></param>
    /// <returns></returns>
    private float CalculateRPM(float wheelVel, int gear)
    {
        return (Mathf.Abs(wheelVel) * 60 * Mathf.Abs(gearRatios[gear]) * finalDriveRatio) / (2 * Mathf.PI);
    }

    /// <summary>
    /// Returns true if the gear is applicable for the current angular velocity of the wheels
    /// </summary>
    /// <param name="gear"></param>
    /// <returns></returns>
    public bool ApplicableGear(int gear)
    {
        if (CalculateRPM(driveWheels[0].wheelAngularVelocity, gear) < maxRpm * 0.8f)
            return true;

        return false;
    }

    /// <summary>
    /// Returns true if moving up to the next gear would be appropriate
    /// </summary>
    /// <returns></returns>
    public bool MoveUp()
    {
        foreach(Wheel w in driveWheels)
        {
            if(carBody.velocity.magnitude < 7f)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Applies the handbrake to the rear wheels
    /// </summary>
    /// <param name="b"></param>
    private void ApplyHandBrake(bool b)
    {
        backLeftWheel.wheelHandbrake = b;
        backRightWheel.wheelHandbrake = b;
    }

    /// <summary>
    /// Returns the value of the Torque curve given rpm x
    /// </summary>
    /// <param name="x">rpm</param>
    /// <returns></returns>
    private float EvaluateTorqueCurve(float x)
    {
        return torqueCurve.Evaluate(x);
    }

    /// <summary>
    /// Calculate and applies aerodynamic forces
    /// </summary>
    private void ApplyAero()
    {
        float dc = dragCoefficient;
        if (slipstreaming)
            dc = dragCoefficient / 6;

        Vector3 drag = carBody.velocity * -dc * dragArea * carBody.velocity.magnitude;
        carBody.AddForce(drag);
        Debug.DrawRay(transform.position, drag / (0.5f * carBody.mass), slipstreaming? Color.green: Color.magenta);

        Vector3 forwardVel = Vector3.ProjectOnPlane(Vector3.ProjectOnPlane(carBody.velocity, transform.right), transform.up);
        currentDownforce = 0.5f * downforceCoefficient * 1.15f * downforceArea * Mathf.Pow(forwardVel.magnitude, 2);
        carBody.AddForce(-transform.up * currentDownforce);
        Debug.DrawRay(transform.position, -transform.up * currentDownforce * 0.5f / carBody.mass);
        float wheelDownforce = currentDownforce / 4;

        frontLeftWheel.SetDownforce(wheelDownforce);
        frontRightWheel.SetDownforce(wheelDownforce);
        backLeftWheel.SetDownforce(wheelDownforce);
        backRightWheel.SetDownforce(wheelDownforce);
    }

    /// <summary>
    /// Sets whether the car should be frozen. Freezing the car stops it from moving.
    /// </summary>
    /// <param name="f"></param>
    public void SetFreeze(bool f)
    {
        frozen = f;
    }

    //Called by Unity when the vehicle enters a Unity Trigger region
    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Slipstream")
        {
            slipstreamTimeout = Time.time + 0.2f;
        }
    }

    //Called by Unity, used to render UI and HUD
    private void OnGUI()
    {
        if (!debugUI)
            return;

        string guiGear = (curGear < 0)? "R" : (curGear + 1).ToString();
        GUI.Label(new Rect(0,0,200,25), "Mph: " + Mathf.Round(mph) + " Rpm: " + curRpm + " Gear: " + guiGear);
    }
}
