﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WaypointAutoAdd : MonoBehaviour
{

    private WaypointTrack track;
    public bool autoAdd;

    // Update is called once per frame
    void Update()
    {
        if (track == null)
        {
            track = GetComponent<WaypointTrack>();
            return;
        }
            

        if(track.waypoints.Count != transform.childCount)
        {
            //Update
            if (!autoAdd)
                return;

            track.waypoints = new List<Transform>();

            for (int i = 0; i < transform.childCount; ++i)
            {
                track.waypoints.Add(transform.GetChild(i));
            }
        }
    }
}
