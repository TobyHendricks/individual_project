﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class WaypointAutoHeight : MonoBehaviour
{
    public bool place;

    // Update is called once per frame
    void Update()
    {
        if (place)
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position + Vector3.up * 30, Vector2.down, out hit))
            {
                Vector3 pos = hit.point;
                pos.y += 0.5f;
                transform.position = pos;
            }
            place = false;
        }
    }
}
