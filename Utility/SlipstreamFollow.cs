﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlipstreamFollow : MonoBehaviour
{
    public Transform followTarget;
    public float lerp;
    Vector3 currentPos;

    private void Start()
    {
        currentPos = transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        transform.LookAt(followTarget);
        currentPos = Vector3.Lerp(currentPos, followTarget.position, lerp * Time.deltaTime);
        currentPos.y = transform.parent.position.y;
        transform.localPosition = transform.parent.InverseTransformPoint(currentPos);
    }
}
