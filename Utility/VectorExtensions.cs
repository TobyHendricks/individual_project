﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorExtensions
{
    public static Vector2 Rotate(this Vector2 input, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        Vector2 output = input;
        output.x = (cos * input.x) - (sin * input.y);
        output.y = (sin * input.x) + (cos * input.y);
        return output;

    }
}

