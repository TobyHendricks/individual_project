﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the players HUD during a race
/// </summary>
[RequireComponent(typeof(RacePositionHandler))]
public class PlayerRaceHUD : MonoBehaviour
{
    public RaceCompletedUI completionUI;
    private RacePositionHandler racePositionHandler;

    private void Start()
    {
        racePositionHandler = GetComponent<RacePositionHandler>();
        racePositionHandler.OnFinishedRace += OnFinish;
    }

    public void OnFinish(int place)
    {
        RaceCompletedUI instance = Instantiate(completionUI);
        instance.Initialise(RaceManager.PositionToString(place));
    }

    private void OnDestroy()
    {
        racePositionHandler.OnFinishedRace -= OnFinish;
    }
}
