﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Displays relevant UI when the race completes
/// </summary>
public class RaceCompletedUI : MonoBehaviour
{
    public Text completionText;

    public void Initialise(string placeText)
    {
        completionText.text = "RACE COMPLETED\n\n" + placeText;
    }
}
