﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Displays the HUD of the current vehicle 
/// </summary>
public class UserHUDManager : MonoBehaviour
{
    public static UserHUDManager HUD;

    public Image rpmImage;
    public Text rpmText;
    public Text mphText;
    public Text gearText;

    //Singleton
    #region Singleton
    private void Awake()
    {
        UserHUDManager.HUD = this;
    }

    public static UserHUDManager GetHUD()
    {
        return UserHUDManager.HUD;
    }
    #endregion

    public void SetRpm(float rpm, float minRpm, float maxRpm)
    {
        rpmText.text = "RPM: " + Mathf.Round(rpm).ToString();

        float ratio = (rpm - minRpm) / (maxRpm - minRpm);
        rpmImage.fillAmount = ratio;
        rpmImage.color = Color.Lerp(Color.white, Color.red, Mathf.Pow(ratio, 6));
    }

    public void SetGear(int gear)
    {
        string gearAsText = (gear < 0) ? "R" : (gear + 1).ToString();
        gearText.text = "Gear: " + gearAsText;
    }

    public void SetMph(float mph)
    {
        mphText.text = "MPH: " + Mathf.Round(mph).ToString();
    }

    
}
