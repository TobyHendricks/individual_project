﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sends the details of a vehicle to the User HUD
/// </summary>
public class VehicleHUD : MonoBehaviour
{
    private VehicleController vehicle;
    private UserHUDManager HUD;

    // Start is called before the first frame update
    void Start()
    {
        HUD = UserHUDManager.GetHUD();
        vehicle = GetComponent<VehicleController>();
    }

    private void Update()
    {
        if (HUD == null)
            return;

        HUD.SetRpm(vehicle.curRpm, vehicle.minRpm, vehicle.maxRpm);
        HUD.SetGear(vehicle.curGear);
        HUD.SetMph(vehicle.speed * 2.237f);
    }
}
