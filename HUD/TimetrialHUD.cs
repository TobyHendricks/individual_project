﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manages the HUD of the time trial
/// </summary>
public class TimetrialHUD : MonoBehaviour
{
    public Text currentTimeText;
    public Text bestTimeText;
    public Text lastTimeText;

    public void SetCurrentTime(string timeAsString)
    {
        currentTimeText.text = "TIME: " + timeAsString;
    }

    public void SetBestTime(string timeAsString)
    {
        bestTimeText.text = "BEST: " + timeAsString;
    }

    public void SetLastTime(string timeAsString)
    {
        lastTimeText.text = "LAST: " + timeAsString;
    }
}