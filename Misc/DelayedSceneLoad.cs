﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DelayedSceneLoad : MonoBehaviour
{
    public float delay;
    public int sceneIndex;

    // Start is called before the first frame update
    void Start()
    {
        Invoke(nameof(Load), delay);
    }

    // Update is called once per frame
    void Load()
    {
        SceneManager.LoadScene(sceneIndex);
    }
}
