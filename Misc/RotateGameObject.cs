using UnityEngine;

public class RotateGameObject : MonoBehaviour 
{
	public Vector3 rotateSpeed;
	
	// Update is called once per frame
	void FixedUpdate () 
	{
			transform.Rotate(rotateSpeed * Time.fixedDeltaTime, Space.World);
	}
}
