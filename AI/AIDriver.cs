﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDriver : MonoBehaviour, VehicleInputInterface
{
    private Rigidbody body;
    private Collider carCollider;
    private VehicleController vehicle;
    private Vector3 targetPos;
    private Transform target;
    private Transform nextTarget;
    private int targetIndex;
    private WaypointTrack track;

    [Header("Steering")]
    public float proportionalCoefficient;
    public float derivativeCoefficient;
    [Tooltip("x = speed, y = max steer")]
    public AnimationCurve maxSteerCurve;

    private float steerInput;
    private float deltaSteerAngle;
    private float angleToTarget;

    [Header("Speed")]
    public float accelerateThreshold;
    public float brakeThreshold;
    public float coastThreshold;

    //speed variables
    public short speedStage;
    private float forwardInput;

    //Gear Change
    private float nextGearChange = 0;
    private short gearChangeBuffer = 0;

    [Header("Target Speed Management")]
    public float maxSpeed = 70;
    public float minSpeed = 6;
    public float angleToCurrentWaypointCoefficient;
    public float cornerPrediction;
    public float[] cornerCoefficients;
    public float[] cornerCoefficientMaxSpeed;
    private float[] cornerCoefficientWeight;
    public float maxCurvature;

    //AI status
    private float currentAngleToWay;
    private float targetSpeed;
    private float targetCornerSpeed;
    private float currentWaypointSpeed;

    //Prediction constants
    private const float cornerPredictionCoefficient = 70f;
    private const float velocityPower = 0.8f;

    [Header("Pathing")]
    public float waypointDistanceThreshold;

    [Header("Overtake Detection")]
    public LayerMask vehicleMask;
    public float detectionMinRange;
    public float detectionMaxRange;
    public float detectionTime;
    public float detectionThickness;

    [Header("Overtake Parameters")]
    public float behindAngleThreshold;
    public float overtakeDirectionWeight;
    public float overtakePositionWeight;
    public float overtakeLerp;
    public float tooCloseDistance;
    public float tooCloseTime;
    public float breakApartWeight;
    public float breakApartDistance;
    public float emergencyStopTime;
    public float overtakeSpeedBoost = 1;

    [Header("Driving Assist")]
    [Tooltip("Thresholds for each gear")]
    public float[] steerInputAccelThresholds;
    public float steerInputAccelThreshold
    {  get { return steerInputAccelThresholds[Mathf.Clamp(vehicle.curGear, 0, steerInputAccelThresholds.Length - 1)]; } }

    [Tooltip("Thresholds for each gear")]
    public float[] turningVelocityThresholds;
    public float turningVelocityThreshold 
    { get { return turningVelocityThresholds[Mathf.Clamp(vehicle.curGear, 0, turningVelocityThresholds.Length - 1)]; } }

    public float pedalLiftLerp = 60;

    public float[] pedalFloorLerps;
    public float pedalFloorLerp 
    { get { return pedalFloorLerps[Mathf.Clamp(vehicle.curGear, 0, pedalFloorLerps.Length - 1)]; } }

    private Vector3 overtakeVector;
    private Vector3 overtakeOffset;
    private bool overtaking;
    private float currentSpeedBoost;
    private int overtakeBufferID;
    private Rigidbody overtakeBuffer;
    private bool tooClose;
    private bool emergencyStop;

    //Start is called by Unity before the first frame update
    private void Start()
    {
        body = GetComponent<Rigidbody>();
        carCollider = GetComponentInChildren<Collider>();
        vehicle = GetComponent<VehicleController>();
        track = GameObject.FindGameObjectWithTag("Track").GetComponent<WaypointTrack>();

        cornerCoefficientWeight = new float[cornerCoefficients.Length];

        InitialiseWaypoint();
    }

    /// <summary>
    /// Initialises the waypoints
    /// </summary>
    public void InitialiseWaypoint()
    {
        targetIndex = track.ClosestWaypointIndex(transform.position);
        target = track.GetWaypoint(targetIndex);
        targetPos = target.position;
        nextTarget = track.NextWaypoint(targetIndex);
    }

    //Update is called each frame
    private void Update()
    {
        CalculateOvertakes();
        targetPos = Vector3.Lerp(targetPos, target.position, 1.3f * Time.deltaTime);
    }

    //Fixed Update is called by Unity a fixed number of times per second
    private void FixedUpdate()
    {
        UpdateWaypoints();
        Steer();
        UpdateSpeed();
    }

    [Header("Steering Debug")]
    public float kp;
    public float kd;
    public float pd;

    /// <summary>
    /// Steers the AI vehicle
    /// </summary>
    private void Steer()
    {

        Vector3 p = targetPos + overtakeOffset;
        Vector3 targetDirection = Vector3.Normalize(p - transform.position);
        targetDirection += overtakeVector;

        Debug.DrawRay(transform.position, targetDirection.normalized * 10);

        float newAngle = Vector3.SignedAngle(body.velocity, targetDirection, Vector3.up);
        currentAngleToWay = newAngle;
        deltaSteerAngle = newAngle - angleToTarget;
        angleToTarget = newAngle;

        float s = 0;
        if (Mathf.Abs(newAngle) > 1)
            s = angleToTarget * proportionalCoefficient + derivativeCoefficient * deltaSteerAngle / Time.fixedDeltaTime;

        kp = angleToTarget * proportionalCoefficient;
        kd = derivativeCoefficient * deltaSteerAngle / Time.fixedDeltaTime;
        pd = kp + kd;

        float maxSteer = maxSteerCurve.Evaluate(body.velocity.magnitude);
        steerInput = Mathf.Clamp(s, -1, 1) * maxSteer;
    }

    /// <summary>
    /// Calculates the target speed, attempts to match it and tries to prevent oversteer/understeer
    /// </summary>
    private void UpdateSpeed()
    {
        //Target speeds
        float a1 = Mathf.Pow(Vector3.Angle(body.velocity, target.position - transform.position), 2);
        currentWaypointSpeed = maxSpeed - angleToCurrentWaypointCoefficient * a1;

        float total = 0;
        int numIts = 0;
        Vector3 start = transform.position;
        Transform followingPoint = track.NextWaypoint(targetIndex);
        for(int i = 0; i < cornerCoefficients.Length; ++i)
        {
            Vector3 targetPos = track.GetWaypoint(targetIndex + i).position;
            Vector3 futurePos = track.GetWaypoint(targetIndex + i + 1).position;
            float dist = Vector3.Distance(transform.position, futurePos);

            bool consider = Mathf.Pow(dist, 2) / Mathf.Pow(Mathf.Max(1, body.velocity.magnitude), velocityPower) < cornerPrediction * cornerPredictionCoefficient
                && cornerCoefficientMaxSpeed[i] < body.velocity.magnitude;
            if (consider)
            {
                ++numIts;
                cornerCoefficientWeight[i] = Mathf.Lerp(cornerCoefficientWeight[i], 1, Time.deltaTime * 0.5f);

                float curvature = CalculateCurvature(start, targetPos, futurePos);
                total += Mathf.Max(0, maxCurvature - curvature) * cornerCoefficients[i] * cornerCoefficientWeight[i];
                
                Debug.DrawRay(futurePos, Vector3.up * 5, new Color(1,1,0));
            }
            else
            {
                for(;i < cornerCoefficients.Length; ++i)
                {
                    cornerCoefficientWeight[i] = Mathf.Lerp(cornerCoefficientWeight[i], 0, Time.deltaTime * 2); ;
                }
                break;
            }
                

            start = targetPos;
        }

        targetCornerSpeed = maxSpeed - total;


        currentSpeedBoost = Mathf.Lerp(currentSpeedBoost, overtaking ? 0 : overtakeSpeedBoost, Time.deltaTime * 0.4f);
        targetSpeed = Mathf.Min(currentWaypointSpeed, targetCornerSpeed);
        targetSpeed = Mathf.Clamp(targetSpeed, minSpeed + currentSpeedBoost, maxSpeed + currentSpeedBoost / 2);
        

        //Accelerating
        if (speedStage == 1)
        {
            if (body.velocity.magnitude > targetSpeed - accelerateThreshold)
                speedStage = 0;

            if (emergencyStop)
                forwardInput = -1;
            else if (tooClose)
                forwardInput = -0.1f;
            else if (targetSpeed > 15)
            {
                bool turning = Mathf.Abs(steerInput) > steerInputAccelThreshold;
                float f = Mathf.Lerp(forwardInput, turning? -0.6f : 1.065f, turning? pedalLiftLerp * Time.fixedDeltaTime : pedalFloorLerp * Time.fixedDeltaTime);
                forwardInput = Mathf.Clamp(f, 0.3f, 1);
            } 
            else
                forwardInput = 0.5f;
        }
        else if (speedStage == 0)//Coasting
        {
            if (body.velocity.magnitude < targetSpeed - coastThreshold)
                speedStage = 1;
            else if (body.velocity.magnitude > targetSpeed + coastThreshold)
                speedStage = -1;

            if (emergencyStop)
                forwardInput = -1;
            else if (tooClose)
                forwardInput = -0.3f;
            else
                forwardInput = 0;
        }
        else //Braking
        {
            if (body.velocity.magnitude < targetSpeed + brakeThreshold)
                speedStage = 0;
            
            forwardInput = Mathf.Abs(currentAngleToWay) > 15? -0.35f : -1;
        }

        //Gears
        if(vehicle.curRpm < vehicle.maxRpm - 300 && speedStage == -1 && vehicle.curGear > 0 && vehicle.ApplicableGear(vehicle.curGear - 1))
        {
            QueueGearChange(-1);
        }
        else if(vehicle.curRpm > vehicle.maxRpm - 150 && speedStage == 1)
        {
            if (!vehicle.MoveUp())
                QueueGearChange(1);
        }
    }

    /// <summary>
    /// Queues a gear change to occur as soon as possible
    /// </summary>
    /// <param name="dir"></param>
    private void QueueGearChange(short dir)
    {
        if (Time.time > nextGearChange)
        {
            gearChangeBuffer = dir;
        }
    }

    /// <summary>
    /// Updayes the AIs current waypoints
    /// </summary>
    private void UpdateWaypoints()
    {
        if(Vector3.Distance(transform.position, target.position) < waypointDistanceThreshold)
        {
            targetIndex = track.NextIndex(targetIndex);
            target = nextTarget;
            nextTarget = track.NextWaypoint(targetIndex);
        }
    }

    /// <summary>
    /// Calculates overtake vectors
    /// </summary>
    private void CalculateOvertakes()
    {
        //Detect nearby cars
        float range = Mathf.Clamp(body.velocity.magnitude * detectionTime, detectionMinRange, detectionMaxRange);
        Vector3 extents = new Vector3(detectionThickness, 3, range);
        Vector3 origin = transform.position + transform.forward * range * 0.5f;

        Debug.DrawRay(origin + transform.right * extents.x - transform.forward * extents.z, transform.forward * extents.z * 2);
        Debug.DrawRay(origin - transform.right * extents.x - transform.forward * extents.z, transform.forward * extents.z * 2);

        Collider[] carColliders = Physics.OverlapBox(origin, extents, Quaternion.LookRotation(body.velocity), vehicleMask);
        Collider closest = null;
        float closestDistance = Mathf.Infinity;

        //Find the closest car
        foreach (Collider col in carColliders)
        {
            if (col.gameObject.GetInstanceID() == carCollider.gameObject.GetInstanceID())
                continue;

            if(overtakeBufferID != col.gameObject.GetInstanceID())
            {
                overtakeBuffer = col.GetComponentInParent<Rigidbody>();
                overtakeBufferID = col.gameObject.GetInstanceID();
            }

            if (Vector3.Distance(transform.position, col.transform.position) < closestDistance)
                closest = col;
        }

        Vector3 otv = Vector3.zero;
        bool overtake = false;
        bool behind = false;
        bool close = false;
        bool breakApart = false;
        bool et = false;
        float dot = 0;

        if(closest != null)
        {
            float mySpeed = body.velocity.magnitude;
            float opponentSpeed = overtakeBuffer.velocity.magnitude;

            behind = Vector3.Angle(transform.forward, closest.transform.position - transform.position) < behindAngleThreshold;

            Vector3 relativePos = transform.InverseTransformPoint(closest.transform.position);
            float t = (relativePos.z / Mathf.Max(0.01f, mySpeed - opponentSpeed));
            et = t < emergencyStopTime && behind;
            close = (t < tooCloseTime || relativePos.z < tooCloseDistance)
                && behind;

            breakApart = Mathf.Abs(relativePos.x) < breakApartDistance && !behind && Mathf.Abs(relativePos.z) < tooCloseDistance;

            overtake = body.velocity.magnitude - 3.6f > overtakeBuffer.velocity.magnitude;
            otv = Vector3.Normalize(Vector3.ProjectOnPlane(transform.position - closest.transform.position, transform.forward));

            dot = Vector3.Dot(body.velocity.normalized, overtakeBuffer.velocity.normalized);
            dot = Mathf.Clamp(dot, 0, 0.5f);

            Debug.DrawLine(transform.position, closest.transform.position, behind? close? Color.red : Color.magenta : breakApart? Color.red : Color.white);
        }
  
        tooClose = close;
        emergencyStop = et;


        if (behind)
            overtakeVector = otv * overtakeDirectionWeight * 1.4f * body.velocity.magnitude / overtakeBuffer.velocity.magnitude;
        else if (overtake)
            overtakeVector = otv * overtakeDirectionWeight * Mathf.Pow(1 - dot, 3);
        else if (breakApart)
            overtakeVector = otv * breakApartWeight;
        else
            overtakeVector = Vector3.Lerp(overtakeVector, Vector3.zero, overtakeLerp * Time.deltaTime);

        if (overtake)
            overtakeOffset = otv * overtakePositionWeight;
        else
            overtakeOffset = Vector3.Lerp(overtakeOffset, Vector3.zero, overtakeLerp * Time.deltaTime);
    }

    /// <summary>
    /// Resets the gear system
    /// </summary>
    public void ResetGear()
    {
        vehicle.curGear = 0;
        vehicle.ResetSteering();
    }

    /// <summary>
    /// Calculates the curvature of the track
    /// </summary>
    /// <param name="startPoint">The position of the vehicle (or the future intended position)</param>
    /// <param name="pointA">The first waypoint</param>
    /// <param name="pointB">The second waypoint</param>
    /// <returns></returns>
    private float CalculateCurvature(Vector3 startPoint, Vector3 pointA, Vector3 pointB)
    {
        return Vector3.Distance(pointA, pointB) / Mathf.Sin(Mathf.Abs(Vector3.SignedAngle(pointA - startPoint, pointB - pointA, Vector3.up)) * Mathf.Deg2Rad);
    }

    #region Input Interface
    //Input interface
    public float ReadHorizontalInput()
    {
        return steerInput;
    }
    public float ReadGasInput()
    {
        return Mathf.Max(forwardInput, 0);
    }
    public bool ReadGearUpInput()
    {

        if(gearChangeBuffer == 1)
        {
            nextGearChange = Time.time + 0.2f;
            gearChangeBuffer = 0;
            return true;
        }

        return false;
    }
    public bool ReadGearDownInput()
    {
        if (gearChangeBuffer == -1)
        {
            nextGearChange = Time.time + 0.2f;
            gearChangeBuffer = 0;
            return true;
        }
        return false;
    }
    public bool ReadHandbrakeInput()
    {
        return false;
    }
    public float ReadBrakeInput()
    {
        return Mathf.Max(-forwardInput, 0);
    }
    #endregion

    //Editor
    //Called by Unity for debugging
    private void OnDrawGizmos()
    {
        if(target != null)
        {
            if (Mathf.Pow(Vector3.Distance(target.transform.position, transform.position), 2) / Mathf.Pow(Mathf.Max(1, body.velocity.magnitude), velocityPower) < cornerPrediction * cornerPredictionCoefficient)
                Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(targetPos, 3f);
            Gizmos.DrawWireSphere(targetPos, waypointDistanceThreshold);
        }
    }
}
